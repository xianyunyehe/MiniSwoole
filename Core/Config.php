<?php
/**
 * Created by PhpStorm.
 * Date: 2018/4/10
 * Time: 10:23
 */

namespace Et;


class Config {

    /**
     * 静态
     *
     * @var array
     */
    protected static $config = [];

    protected static $instance;

    /**
     * Config 构造.
     *
     *传入配置数组或者文件
     *
     * @param $file
     */
    public function __construct ($config = [])
    {
        self::$config = $config;
    }

    /**
     * 取得配置的单例
     *
     * @param array $config
     * @return Config
     */
    public static function getInstance ($config = [])
    {
        if (!self::$instance instanceof self) {
            self::$instance = new Config($config);
        }
        return self::$instance;

    }

    /**
     * 获取配置参数
     *
     * Config::get('web.ip')
     * Config::get('web')
     * @param $key
     * @return mixed|string
     */
    public static function get ($key)
    {
        if(strpos($key,'.') === false) {
            return self::$config[$key] ?? '';
        }

        $keys  = explode('.',$key);

        //最大支持两级
        if(count($keys) == 2) {
            return self::$config[$keys[0]][$keys[1]] ?? '';
        }

        return self::$config[$keys[0]];

    }

    /**
     * 设置配置参数
     *
     * @param $key
     * @param $value
     */
    public static function set ($key, $value)
    {
        self::$config[$key] = $value;
    }

    /**
     * 获取全部的配置
     * @return array
     */
    public static function all()
    {
        return self::$config;
    }

}