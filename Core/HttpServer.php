<?php
/**
 * Created by PhpStorm.
 * Date: 2018/4/10
 * Time: 10:46
 */

namespace Et;

class HttpServer extends Server {

    protected $server = '';

    public function isStart ()
    {
        $this->server->start();
    }

    /**
     * 创建服务
     *
     * @return object \swoole_http_server
     * @throws \Exception
     */
    public function createSever ()
    {
        $host = Config::get('web')['ip'];
        $port = Config::get('web')['port'];
        if ($host == '' || $port == '') {
            throw new \Exception('端口或host没有配置');
        }
        //创建http server
        $this->server = new \swoole_http_server($host, $port);
        $register     = new Register($this->server, 'http');
        $register->register();
        $this->server->start();
        return $this->server;
    }

}