<?php
/**
 * Created by PhpStorm.
 * Date: 2018/4/10
 * Time: 17:59
 */

namespace Et;


class Route {

    public static $route = [];

    protected static $methods = ["GET","POST","PUT","DELETE","OPTION"];

    private static $instance;

    public function __construct ($config = [])
    {
        self::$route  = $config;
    }

    public static function getInstance ($config = [])
    {
        if(!self::$instance instanceof  self) {
            self::$instance = new self($config);
        }
        return self::$instance;
    }

    /**
     * 添加路由到路由列表
     * @param $method string          http请求方式
     * @param $uri    string          http请求地址
     * @param $handle string|callable http处理方式
     */
    public static function addRoute($method,$uri,$handle)
    {
        if(!in_array($method,self::$methods)) {
            return null;
        }

        if(empty($uri) || empty($handle)) {
            self::$route[$method] = [$uri,$handle];
        }
        return self::$route;
    }

    public static function import ()
    {

    }

    public static function all()
    {
        return self::$route;
    }


}