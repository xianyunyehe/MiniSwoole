<?php
/**
 * Created by PhpStorm.
 * Date: 2018/4/10
 * Time: 13:31
 */

namespace Et;

class Register {

    protected $httpRegister = [
        "request" => "onRequest"
    ];

    protected $websocketRegister = [
        "open"    => "onOpen",
        "message" => "onMessage",
    ];
    protected $tcpRegister = [
        "receive" => "onReceive",
        "connect" => "onConnect",
        "close"   => "onClose",
        "start"   => "onStart"
    ];

    protected $commonRegister = [
        "workerStart" => "onWorkerStart",
    ];
    protected $udpRegister = [];

    protected $registerList = [];

    protected $server = '';

    public function __construct ($server, $type = 'http')
    {
        $this->server       = $server;
        $key                = $type . "Register";
        $this->registerList = array_merge($this->$key, $this->commonRegister);
    }

    /**
     * 注册事件
     *
     */
    public function register ()
    {
        if (empty($this->registerList)) {
            return;
        }
        //设置注册回调
        foreach ($this->registerList as $key => $callback) {
            $this->server->on($key, function () use ($key) {
                $args = func_get_args();
                $this->$key($args);
            });
        }
    }

    /**
     * workerStart事件
     *
     * @param $args
     */
    public function workerStart ($args)
    {
        //var_dump($args);

    }

    /**
     * 处理swoole http request请求
     *
     * [swoole_http_request,swoole_http_response]
     *
     * @param $args array
     */
    public function request ($args)
    {
        $request        = $args[0];
        $response       = $args[1];
        $route = new Dispatch();
        $route->dispatch($request,$response);

    }


}