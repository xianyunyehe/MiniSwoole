<?php
/**
 * Created by PhpStorm.
 * Date: 2018/4/10
 * Time: 10:19
 */
use Etong\Config;

define('APP_PATH', __DIR__ . '/app/');
define('ROOT_PAT', __DIR__);


require_once './vendor/autoload.php';

$events = [
    'a'=>function($a){
        echo $a;
    }
];

$event  = \Etong\Event::getInstance($events);
$events = $event->all();

//触发监听事件
foreach ($events as $key => $value) {
    $event->hook($key);
}

Config::getInstance(include_once './config/server.php');
\Etong\Route::getInstance(include_once './config/route.php');
$server = new Etong\HttpServer();
$server->createSever();